package net.redlightning.mtg;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.nio.ByteBuffer;
import java.util.Base64;

public class Util {
    /**
     * Serialize a Mat to JSON format
     *
     * @param mat the mat to serialize
     * @return string containing JSON
     */
    public static String matToJson(Mat mat) {
        JsonObject obj = new JsonObject();

        if (mat.isContinuous()) {
            int cols = mat.cols();
            int rows = mat.rows();
            int elemSize = (int) mat.elemSize();
            int type = mat.type();

            obj.addProperty("rows", rows);
            obj.addProperty("cols", cols);
            obj.addProperty("type", type);

            // We cannot set binary data to a json object, so:
            // Encoding data byte array to Base64.
            String dataString;

            if (type == CvType.CV_32S || type == CvType.CV_32SC2 || type == CvType.CV_32SC3 || type == CvType.CV_16S) {
                int[] data = new int[cols * rows * elemSize];
                mat.get(0, 0, data);
                dataString = Base64.getEncoder().encodeToString(toByteArray(data));
            } else if (type == CvType.CV_32F || type == CvType.CV_32FC2) {
                float[] data = new float[cols * rows * elemSize];
                mat.get(0, 0, data);
                dataString = Base64.getEncoder().encodeToString(toByteArray(data));
            } else if (type == CvType.CV_64F || type == CvType.CV_64FC2) {
                double[] data = new double[cols * rows * elemSize];
                mat.get(0, 0, data);
                dataString = Base64.getEncoder().encodeToString(toByteArray(data));
            } else if (type == CvType.CV_8U) {
                byte[] data = new byte[cols * rows * elemSize];
                mat.get(0, 0, data);
                dataString = Base64.getEncoder().encodeToString(data);
            } else {
                throw new UnsupportedOperationException("unknown type");
            }
            obj.addProperty("data", dataString);

            Gson gson = new Gson();
            return gson.toJson(obj);
        } else {
            System.out.println("Mat not continuous.");
        }
        return "{}";
    }

    /**
     * Deserialize a Mat from JSON
     *
     * @param json the JSON source
     * @return reconstructed Mat
     */
    public static Mat jsonToMat(String json) {
        JsonObject JsonObject = JsonParser.parseString(json).getAsJsonObject();

        int rows = JsonObject.get("rows").getAsInt();
        int cols = JsonObject.get("cols").getAsInt();
        int type = JsonObject.get("type").getAsInt();

        Mat mat = new Mat(rows, cols, type);

        String dataString = JsonObject.get("data").getAsString();
        if (type == CvType.CV_32S || type == CvType.CV_32SC2 || type == CvType.CV_32SC3 || type == CvType.CV_16S) {
            int[] data = toIntArray(Base64.getDecoder().decode(dataString.getBytes()));
            mat.put(0, 0, data);
        } else if (type == CvType.CV_32F || type == CvType.CV_32FC2) {
            float[] data = toFloatArray(Base64.getDecoder().decode(dataString.getBytes()));
            mat.put(0, 0, data);
        } else if (type == CvType.CV_64F || type == CvType.CV_64FC2) {
            double[] data = toDoubleArray(Base64.getDecoder().decode(dataString.getBytes()));
            mat.put(0, 0, data);
        } else if (type == CvType.CV_8U) {
            byte[] data = Base64.getDecoder().decode(dataString.getBytes());
            mat.put(0, 0, data);
        } else {

            throw new UnsupportedOperationException("unknown type");
        }
        return mat;
    }

    public static byte[] toByteArray(float[] floatArray){
        int times = Float.SIZE / Byte.SIZE;
        byte[] bytes = new byte[floatArray.length * times];
        for(int i=0;i<floatArray.length;i++){
            ByteBuffer.wrap(bytes, i*times, times).putDouble(floatArray[i]);
        }
        return bytes;
    }

    public static float[] toFloatArray(byte[] byteArray){
        int times = Float.SIZE / Byte.SIZE;
        float[] floats = new float[byteArray.length / times];
        for(int i=0;i<floats.length;i++){
            floats[i] = ByteBuffer.wrap(byteArray, i*times, times).getFloat();
        }
        return floats;
    }

    public static byte[] toByteArray(double[] doubleArray){
        int times = Double.SIZE / Byte.SIZE;
        byte[] bytes = new byte[doubleArray.length * times];
        for(int i=0;i<doubleArray.length;i++){
            ByteBuffer.wrap(bytes, i*times, times).putDouble(doubleArray[i]);
        }
        return bytes;
    }

    public static double[] toDoubleArray(byte[] byteArray){
        int times = Double.SIZE / Byte.SIZE;
        double[] doubles = new double[byteArray.length / times];
        for(int i=0;i<doubles.length;i++){
            doubles[i] = ByteBuffer.wrap(byteArray, i*times, times).getDouble();
        }
        return doubles;
    }

    public static byte[] toByteArray(int[] intArray){
        int times = Integer.SIZE / Byte.SIZE;
        byte[] bytes = new byte[intArray.length * times];
        for(int i=0;i<intArray.length;i++){
            ByteBuffer.wrap(bytes, i*times, times).putInt(intArray[i]);
        }
        return bytes;
    }

    public static int[] toIntArray(byte[] byteArray){
        int times = Integer.SIZE / Byte.SIZE;
        int[] ints = new int[byteArray.length / times];
        for(int i=0;i<ints.length;i++){
            ints[i] = ByteBuffer.wrap(byteArray, i*times, times).getInt();
        }
        return ints;
    }
}
