package net.redlightning.mtg;

import com.google.gson.JsonObject;
import nu.pattern.OpenCV;
import org.opencv.core.*;
import org.opencv.features2d.ORB;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Generate image descriptors for all cards that don't already have one in the database
 *
 * @author Michael Isaacson
 * @version 22.8.1
 */
public class ProcessImages {
    private static final int FEATURE_LIMIT = 50;
    private static final Size sz = new Size(265,370);

    /**
     * Generate image descriptors for all cards that don't already have one in the database
     *
     * @param args ignored
     */
    public static void main(String[] args) {
        //OpenCV.loadShared();
        //runFromFiles();
        //runFromDatabase();
        copyToTensor();
    }

    private static void runFromDatabase() {
        Database database = new Database();
        database.connect();
        List<Map<String, String>> cards = database.getCardImageURLs();

        int count = 1;

        for (Map<String, String> card : cards) {
            String cardID = card.get("card_id");
            String imageURI = card.get("normal");

            //Download the image
            try {
                Mat imageMat = readImage(imageURI);
                if (imageURI.toLowerCase(Locale.ROOT).contains(".jpg")) {
                    Imgcodecs.imwrite("images/" + cardID + ".jpg", imageMat);
                } else if (imageURI.toLowerCase(Locale.ROOT).contains(".png")) {
                    Imgcodecs.imwrite("images/" + cardID + ".png", imageMat);
                }

                //Resize the image
                Mat resizedImage = new Mat();
                Imgproc.resize(imageMat, resizedImage, sz, Imgproc.INTER_CUBIC);

                //Generate and save the descriptor to the database
                Mat descriptors = getDescriptors(resizedImage);
                String json = Util.matToJson(descriptors);
                //database.insertDescriptor(cardID, json);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("i", cardID);
                jsonObject.addProperty("d", json);

                System.out.println(cardID + " (" + count + "/" + cards.size() + ")");
                count++;
            } catch (IOException e) {
                System.out.println("Failed to generate mat from URI:" + e.getMessage());
                try {
                    FileWriter myWriter = new FileWriter("failed_images.txt", true);
                    myWriter.write(e.getMessage() + "\n");
                    myWriter.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        database.disconnect();
    }

    private static void runFromFiles() {
        try {
            FileWriter descWriter = new FileWriter("descriptors-50.txt", true);
            File folder = new File("images");
            String[] images = folder.list();
            int count = 0;
            assert images != null;
            for (String imageURI : images) {
                Mat imageMat = Imgcodecs.imread("images/" + imageURI);
                String cardID = imageURI.replace(".jpg", "").replace(".png", "");

                //Resize the image
                Mat resizedImage = new Mat();
                Imgproc.resize(imageMat, resizedImage, sz, Imgproc.INTER_CUBIC);

                //Generate and save the descriptor to the database
                Mat descriptors = getDescriptors(resizedImage);
                String json = Util.matToJson(descriptors);
                //database.insertDescriptor(cardID, json);
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("i", cardID);
                jsonObject.addProperty("d", json);
                descWriter.write(jsonObject + "\n");
                System.out.println(cardID + " (" + count + "/" + images.length + ")");
                count++;
            }
            descWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void copyToTensor() {
        File sourceFolder = new File("images");
        String[] sourceImages = sourceFolder.list();

        for (String filename: sourceImages) {
            try {
                File destFolder = new File("tensor/" + filename.replace(".jpg", "").replace(".png", ""));
                boolean made = destFolder.mkdir();
                if (made) {
                    Path sourcePath = Paths.get("images/" + filename);
                    Path targetPath = Paths.get(destFolder.toPath() + "/" + filename);
                    Files.copy(sourcePath, targetPath);
                }
            } catch (IOException e) {
                System.out.println("DOOM!");
            }
        }
    }

    /**
     * Generate ORB descriptors for an image
     *
     * @param image image mat to describe
     * @return mat of descriptors from detected keypoints
     */
    private static Mat getDescriptors(Mat image) {
        ORB detector = ORB.create(FEATURE_LIMIT);
        MatOfKeyPoint keypoints = new MatOfKeyPoint();
        Mat descriptors = new Mat();
        detector.detect(image, keypoints);
        detector.compute(image, keypoints, descriptors);
        return descriptors;
    }

    /**
     * Read an image file from a URL/URI. Supports web URLs
     *
     * @param imageUrl the URL/URI of the image to read
     * @return Mat of the image, read unchanged.
     * @throws IOException likely if the image cannot be read due to bad URL, offline, etc
     */
    public static Mat readImage(String imageUrl) throws IOException {
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        byte[] imageBytes = is.readAllBytes();
        is.close();

        return Imgcodecs.imdecode(new MatOfByte(imageBytes), Imgcodecs.IMREAD_UNCHANGED);
    }
}
