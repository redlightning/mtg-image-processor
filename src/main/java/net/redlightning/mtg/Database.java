package net.redlightning.mtg;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

/**
 * Manage database interactions. Credentials are read from the conf.properties file, user and password
 *
 * @author Michael Isaacson
 * @version 22.8.1
 */
public class Database {
    private static final String DATABASE_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DATABASE_URL = "jdbc:mysql://redlightning.net:3306/redlight_magic";
    private Connection connection;
    private final Properties properties = new Properties();


    /**
     * Constructor. Also loads the properties file
     */
    public Database() {
        try {
            InputStream input = ClassLoader.getSystemClassLoader().getResourceAsStream("conf.properties");
            properties.load(input);
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    /**
     * Get normal sized image URLs for cards that don't already have image descriptor entries
     *
     * @return list of mapped columns: card_id, normal
     */
    public List<Map<String, String>> getCardImageURLs() {
        List<Map<String, String>> results = null;
        connect();

        //String sql = "SELECT card_id, normal FROM `image_uris` WHERE card_id NOT IN (SELECT card_id FROM image_descriptors)";
        String sql = "SELECT card_id, normal FROM `image_uris`";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            results = resultSetToArrayList(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            disconnect();
        }
        return results;
    }

    /**
     * Insert the descriptor JSON into the database as a text string
     *
     * @param cardID card ID from cards table (column there is id)
     * @param descriptor mat to json converted string
     * @return true if the insert succeeded
     */
    public boolean insertDescriptor(String cardID, String descriptor) {
        if(connection == null) {
             return false;
        }

        boolean insertSuccessful = false;
        String sql = "INSERT INTO image_descriptors (card_id, descriptor) VALUES (?,?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString (1, cardID);
            statement.setString (2, descriptor);
            insertSuccessful =  statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return insertSuccessful;
    }

    /**
     * Convert the resultset to a list of maps. This allows us to use it with the database disconnected
     *
     * @param rs the result set
     * @return list containing a map where the column names are the key, and values are all Strings
     * @throws SQLException connection issue, etc
     */
    private List<Map<String, String>> resultSetToArrayList(ResultSet rs) throws SQLException{
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        List<Map<String, String>> list = new ArrayList<>(62000);
        while (rs.next()){
            HashMap<String, String> row = new HashMap<>(columns);
            for(int i=1; i<=columns; ++i){
                row.put(md.getColumnName(i), rs.getString(i));
            }
            list.add(row);
        }
        return list;
    }

    /**
     * Connect to the database
     */
    public void connect() {
        if (connection == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                connection = DriverManager.getConnection(DATABASE_URL, properties.getProperty("user"), properties.getProperty("password"));
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Close the connection, if it is still open
     */
    public void disconnect() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
